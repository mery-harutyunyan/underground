const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const app = express();
const {v4: uuidv4} = require("uuid");
const oneStationFee = 2;
const redis = require('redis');
const client = redis.createClient({
  url: "redis://localhost:6379",
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

mongoose.connect('mongodb://localhost:27017/metro');

//Cards
const cardSchema = new Schema({
  number: String,
  username: String,
  balance: Number
});

const Card = mongoose.model('cards', cardSchema);


//stations
const StationSchema = new Schema({
  name: String,
  prev: String,
  next: String,
});

const Station = mongoose.model('stations', StationSchema);

app.post('/cards', (req, res) => {
  const {username, balance} = req.body;

  const card = new Card({
    username: username,
    balance: balance,
    number: uuidv4(),
  });
  card.save(function (error) {
    if (error) {
      res.send(error)
    }

    res.send("Card saved successfully")
  });
});

app.put('/cards/add_balance', (req, res) => {
  const {card_number, new_balance} = req.body;

  const filter = {number: card_number};
  const update = {
    $inc: {
      balance: new_balance
    }
  }

  Card.findOneAndUpdate(filter, update, function (err, doc) {
    if (err) {
      res.send(err)
    }

    res.send(doc)
  })
})


//Trips
const TripSchema = new Schema({
  card_number: String,
  start_station: String,
  start_time: Date,
  end_station: String,
  end_time: Date,
  is_completed: Boolean,
  station_counts: Number,
});

const Trip = mongoose.model('trips', TripSchema);

const getStationCounts = async function (startStation, endStation) {
  const stations = await Station.find().exec();
  let stationsList = [];

  for (var i = 1; i < stations.length; i++) {
    stationsList[i - 1] = stations[i].prev;
    stationsList[i] = stations[i].name;
    stationsList[i + 1] = stations[i].next;
  }

  const result = stationsList.filter(e => e);

  return Math.abs(result.indexOf(endStation) - result.indexOf(startStation));
}

app.post('/trips', async (req, res) => {

  const {cardNumber, stationName} = req.body;

  const filter = {number: cardNumber, is_completed: false};

  const trip = await Trip.findOne(filter).exec();
  if (trip) {
    trip.end_station = stationName;
    trip.end_time = new Date();
    trip.is_completed = true;
    trip.station_counts = getStationCounts(trip.start_station, stationName);

    trip.save(function (error) {
      if (error) {
        res.send(error)
      }
      const filter = {number: cardNumber};
      const update = {
        $inc: {
          balance: -trip.station_counts * oneStationFee
        }
      };

      Card.findOneAndUpdate(filter, update, function (err, doc) {
        if (err) {
          res.send(err)
        }

        res.send(doc)
      })
    });
  } else {
    const trip = new Trip({
      card_number: cardNumber,
      start_station: stationName,
      start_time: new Date(),
      is_completed: false,
    });
    trip.save(function (error) {
      if (error) {
        res.send(error)
      }

      res.send("Trip created successfully")
    });
  }
});

app.post('/trips/duration', async (req, res) => {
  const {startStation, endStation} = req.body;

  const avg = await Trip.aggregate([
    {$match: {start_station: startStation, end_station: endStation}},
    {
      "$group": {
        "_id": null,
        "avg": {
          "$avg": {"$subtract": ["$end_time", "$start_time"]}
        }
      }
    }
  ]).exec();

  res.send(avg)
});

app.get('/trips/top10', async (req, res) => {
  const top10 = await Trip.aggregate([
    {
      $group:
        {
          _id: "$card_number",
          totalAmount: {$sum: "$station_counts"}
        }
    }
  ]).sort({totalAmount: -1}).limit(10).exec();


  client.connect();

  const redisTop10 = await client.get('top10');
  if (redisTop10) {
    res.send(redisTop10)
  } else {
    await client.set('top10', top10[0].totalAmount)
    res.send(top10)
  }
});


app.post('/stations', async (req, res) => {
  const {name, prev, next} = req.body;

  const station = new Station({
    name: name,
    prev: prev,
    next: next,
  });
  station.save(function (error) {
    if (error) {
      res.send(error)
    }

    res.send("Station saved successfully")
  });

});


app.listen(3000, () => {
  console.log("Server started");
})